import { setupErrorHandler } from './packages/errorHandler/index.js'
import Action from './update/index.js'

addEventListener('load', main)

/**
 * Program entry point.
 */
function main () {
  setupErrorHandler(event => alert(event.message)) // TODO: to improve with analytics
  window.addEventListener('popstate', Action.popStateEventHandler)
  Action.refreshAllScreens()
}
