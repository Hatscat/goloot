import { createProfile, isNewProfile } from '../data/index.js'
import { createElement } from '../packages/element/index.js'
import Action, { Screen } from '../update/index.js'
import { createHeader, createTitle } from './header.js'
import { createProfileCard } from './profile-card.js'
import { Spacing } from './theme.js'
import { index, t } from './translations.js'

/**
 * @typedef {import('../packages/element').Lm} Lm
 * @typedef {import('../data/profile').Profile} Profile
 */

/**
 * @returns {Lm & {onMount: () => void}}
 */
export function createChooseActiveProfileScreen () {
  const profileCardsContainer = createElement('div', {
    style: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap'
      // justifyContent: 'space-evenly'
    }
  })
  const screen = createElement('div', null,
    [
      createHeader(createTitle(t(index.chooseActiveProfileTitle))),
      profileCardsContainer
    ])

  return Object.assign(screen, {
    onMount: () => {
      const profileIds = Action.getProfileIds()
      if (profileIds) {
        /** @type {Profile[]} */
        const existingProfiles = profileIds.reduce(
          /** @param {Profile[]} result */
          (result, id) => {
            const profile = Action.getProfile(id)
            return profile ? result.concat(profile) : result
          }, [])
        const profiles = existingProfiles.some(isNewProfile)
          ? existingProfiles
          : existingProfiles.concat(createProfile())
        const profileCards = profiles.map(profile => createProfileCard(profile, profileCardClickHandler, t(index.newProfileCta), { style: { margin: Spacing.layout(2) } }))

        profileCardsContainer.replaceContent(profileCards)
      }
    }
  })
}

/**
 * @param {Profile} profile
 */
function profileCardClickHandler (profile) {
  if (isNewProfile(profile)) {
    Action.navigateToScreen(Screen.editProfile(profile))
  } else {
    Action.setActiveProfileId(profile.uid)
    Action.navigateToScreen(Screen.main())
  }
}
