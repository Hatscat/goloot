import Config from '../config.js'
import { availableLocales } from '../data/locale.js'
import { createElement } from '../packages/element/index.js'
import Action from '../update/index.js'
import { createHeader, createTitle } from './header.js'
import { index, t } from './translations.js'

export function createSettingsScreen () {
  const screen = createElement('div', null, [
    createHeader(createTitle(t(index.settingsTitle)), {
      backButton: true
    }),
    createElement('div', {}, [
      createElement('label', {
        style: {
          float: 'left', // ?
          textAlign: 'right'// ?
        }
      }, t(index.chooseLocale)),
      createElement('select', {
        onchange: event => {
          if (event.target) {
            // @ts-ignore
            Action.setLocale(event.target.value)
          }
        }
      },
      availableLocales.map(locale =>
        createElement('option', {
          value: locale,
          selected: locale === Action.getOrCreateLocale()
        }, locale))
      )
    ]),
    Config.isDevMode
      ? createElement('div', null, [
        createElement('button', {
          onclick: () => {
            Action.removeAllStoredData()
            Action.refreshAllScreens()
          }
        }, 'Remove All Stored Data')
      ]) : null
  ])

  return screen
}
