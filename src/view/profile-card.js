import { createElement } from '../packages/element/index.js'
import { Size } from './theme.js'

/**
 * @typedef {import('../packages/element').Lm} Lm
 * @typedef {import('../data/profile').Profile} Profile
 */

/**
 * @param {Profile} profile
 * @param {(profile: Profile) => void} onClick
 * @param {string} defaultName
 * @param {{style?: Partial<CSSStyleDeclaration>}} [options]
* @returns {Lm & {update: (value: Profile | null) => void}} profileCard
 */
export function createProfileCard (profile, onClick, defaultName, options) {
  const card = createElement('button', {
    onclick: () => onClick(profile),
    style: {
      cursor: 'pointer',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      backgroundColor: profile.color,
      ...(options || {}).style
    }
  },
  createProfileCardBody(profile, defaultName))

  return Object.assign(card, {
    /** @param {Profile | null} value */
    update: value => {
      if (!value) { return }
      card.onclick = () => onClick(value)
      card.style.backgroundColor = value.color
      card.replaceContent(createProfileCardBody(value, defaultName))
    }
  })
}

/**
 * @param {Profile} profile
 * @param {string} defaultName
 */
function createProfileCardBody (profile, defaultName) {
  return [
    createElement('h5', {
      style: {
        fontSize: Size.icon(2)
      }
    }, profile.emoji),
    createElement('h5', null, profile.name == null ? defaultName : profile.name)
  ]
}
