/** * @readonly */
export const Spacing = {
  nil: '0px',
  /** @param {number} n */
  layout: n => `${n << 3}px`,
  /** @param {number} n */
  text: n => `${n << 2}px`
}

/** * @readonly */
export const Color = {
  /** TODO: rework folloing Material rules: https://material.io/design/color/the-color-system.html#color-usage-palettes */
  primary: '#FFB300',
  secondary: '#FFEE58',
  background: '#FFFFF0'
}

/** * @readonly */
export const Size = {
  /** @param {number} n */
  icon: n => `${n << 4}px`
}
