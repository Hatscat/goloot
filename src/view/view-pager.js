import { createElement } from '../packages/element/index.js'

/**
 * @typedef {import('../packages/element').Lm} Lm
 */

/**
 * @param {Lm[]} pageBodies
 * @return {Lm & {goToPageIndex: (pageIndex: number) => void, addPageChangeListener: (handler: (pageIndex: number) => void) => void }}
 */
export function createViewPager (pageBodies) {
  let pageIndex = 0
  /** @type {((pageIndex: number) => void)[]} */
  const pageChangeHander = []
  const viewPager = createElement('div', {
    style: {
      display: 'flex',
      // flexDirection: 'row',
      width: '100%',
      height: '100%',
      // @ts-ignore
      scrollSnapType: 'x mandatory',
      overflowX: 'scroll',
      overflowScrolling: 'touch'
      // overflow: 'hidden'
    },
    onscroll: event => {
      // console.log('>>>', viewPager.scrollLeft, viewPager.clientWidth / viewPager.scrollLeft, event.target)
      if (pageChangeHander.length > 0) {
        const newPageIndex = Math.round(viewPager.scrollLeft / viewPager.clientWidth)
        if (newPageIndex !== pageIndex) {
          pageIndex = newPageIndex
          pageChangeHander.forEach(handler => handler(newPageIndex))
        }
      }
    }
  },
  pageBodies.map(
    body => createElement('section', {
      style: {
        // @ts-ignore
        scrollSnapAlign: 'start',
        minWidth: '100%',
        height: '100%'
      }
    }, body)
  ))

  return Object.assign(viewPager, {
    /** @param {number} pageIndex */
    goToPageIndex: pageIndex => {
      viewPager.scrollTo({ left: viewPager.clientWidth * pageIndex, behavior: 'smooth' })
    },
    /** @param {(pageIndex: number) => void} handler */
    addPageChangeListener: handler => {
      pageChangeHander.push(handler)
    }
  })
}
