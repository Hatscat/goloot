import { createElement } from '../packages/element/index.js'
import Action from '../update/index.js'
import { createHeader, createTitle } from './header.js'
import { index, t } from './translations.js'

/**
 * @typedef {import('../packages/element').Lm} Lm
 * @typedef {import('../data/profile').Profile} Profile
 */

/**
* @returns {Lm & {onMount: (profile: Profile) => void}}
*/
export function createEditProfileScreen () {
  const formContainer = createElement('div')
  const screen = createElement('div', null, [
    createHeader(createTitle(t(index.editProfileTitle)), {
      backButton: true
    }),
    formContainer
  ])

  return Object.assign(screen, {
    /** @param {Profile} profile */
    onMount: profile => {
      formContainer.replaceContent(createProfileForm(profile))
    }
  })
}

/**
 * @param {Profile} profile
 */
function createProfileForm (profile) {
  return createElement('div', {}, [
    createElement('div', {}, [
      createElement('label', {}, t(index.profileNameLabel)),
      createElement('input', {
        value: profile.name,
        minLength: 2,
        maxLength: 8,
        onchange: event => {
          if (event.target) {
            // @ts-ignore
            const name = event.target.value.trim()
            Action.editProfile(profile.uid, { name })
          }
        }
      })
    ]),
    createElement('div', {}, [
      createElement('label', {}, t(index.profileEmojiLabel)),
      // TODO: emoji picker!
      createElement('input', {
        value: profile.emoji,
        minLength: 1,
        maxLength: 2,
        onchange: event => {
          if (event.target) {
            // @ts-ignore
            const emoji = event.target.value
            Action.editProfile(profile.uid, { emoji })
          }
        }
      })
    ]),
    createElement('div', {}, [
      createElement('label', {}, t(index.profileColorLabel)),
      createElement('input', {
        type: 'color',
        value: profile.color,
        pattern: '#[0-9a-fA-F]{6}',
        onchange: event => {
          if (event.target) {
            // @ts-ignore
            const color = event.target.value
            Action.editProfile(profile.uid, { color })
          }
        }
      })
    ])
  ])
}
