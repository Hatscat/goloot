import { createElement } from '../packages/element/index.js'
import Action from '../update/index.js'
import { Color, Size, Spacing } from './theme.js'

/**
 * @typedef {import('../packages/element').Lm} Lm
 */

/**
 * @param {Lm | Lm[] } body
 * @param {{ backButton?: boolean, onBack?: () => void }} [options]
 * @returns {Lm} header
 */
export function createHeader (body, options) {
  const header = createElement('div', {
    style: {
      paddingTop: Spacing.layout(12)
    }
  },
  createElement('div', {
    style: {
      position: 'fixed',
      left: Spacing.nil,
      top: Spacing.nil,
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingTop: Spacing.layout(2),
      paddingBottom: Spacing.layout(1),
      backgroundColor: Color.background
    }
  // @ts-ignore
  }, options != null && options.backButton
    ? createBackButtonWrapper(body, options.onBack)
    : Array.isArray(body)
      ? body
      : [createElement('div'), body, createElement('div')])
  )

  return header
}

/**
 * @param {string} title
 * @returns {Lm}
 */
export function createTitle (title) {
  return createElement('h4', {
    style: {
      textAlign: 'center'
    }
  }, title)
}

/**
 * @param {Lm | Lm[]} bodyElement
 * @param {() => void} [onBack]
 */
function createBackButtonWrapper (bodyElement, onBack) {
  return [
    createElement('i', {
      className: 'material-icons',
      style: {
        fontSize: Size.icon(2),
        cursor: 'pointer',
        marginLeft: Spacing.layout(3)
      },
      onclick: () => {
        if (onBack) {
          onBack()
        }
        Action.navigateBack()
      }
    }, 'arrow_back'),
    bodyElement,
    createElement('div', {
      style: {
        width: Size.icon(2),
        marginRight: Spacing.layout(3)
      }
    })
  ]
}
