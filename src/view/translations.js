import { translator } from '../packages/translator/index.js'
import Action from '../update/index.js'

// TODO: to sort!
const translationKeys = {
  mainTitle: '',
  settingsTitle: '',
  chooseActiveProfileTitle: '',
  editProfileTitle: '',
  chooseLocale: '',
  newProfileCta: '',
  profileNameLabel: '',
  profileEmojiLabel: '',
  profileColorLabel: '',
  continueButton: '',
  missions: '',
  rewards: '',
  events: ''
}

const en = {
  mainTitle: 'Goloot',
  settingsTitle: 'Settings',
  chooseActiveProfileTitle: 'Who is it?',
  editProfileTitle: 'Manage profile',
  chooseLocale: 'Language:',
  newProfileCta: 'New profile',
  profileNameLabel: 'Name (8 characters maximum):',
  profileEmojiLabel: 'Avatar:',
  profileColorLabel: 'Color:',
  continueButton: 'Continue',
  missions: 'Missions',
  rewards: 'Rewards',
  events: 'Events'
}

const fr = {
  mainTitle: 'Goloot',
  settingsTitle: 'Paramètres',
  chooseActiveProfileTitle: 'Qui est-ce ?',
  editProfileTitle: 'Éditer le profil',
  chooseLocale: 'Langue :',
  newProfileCta: 'Nouveau profil',
  profileNameLabel: 'Nom (8 caractères maximum) :',
  profileEmojiLabel: 'Portrait :',
  profileColorLabel: 'Couleur :',
  continueButton: 'Continuer',
  missions: 'Missions',
  rewards: 'Récompenses',
  events: 'Journal'
}

export const { t, index } = translator(translationKeys, { en, fr }, () => Action.getOrCreateLocale())
