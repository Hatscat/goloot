import { createElement } from '../packages/element/index.js'
import Action, { Screen } from '../update/index.js'
import { createHeader } from './header.js'
import { createProfileCard } from './profile-card.js'
import { createProfileViewPager } from './profile-view-pager.js'
import { Size, Spacing } from './theme.js'

/**
 * @typedef {import('../packages/element').Lm} Lm
 * @typedef {import('../data/profile').Profile} Profile
 */

export function createMainScreen () {
  /** @type {Record<string, Lm>} */
  const profileViewPagersById = {}
  const initialActiveProfile = Action.getOrCreateActiveProfile()

  const profileViewPagerContainer = createElement('main', {
    style: {
      margin: '0 auto',
      width: '100%'
    }
  }, createProfileViewPager(initialActiveProfile))

  /** @param {Profile} profile */
  const profileCardClickHandler = profile => {
    if (profile === null) { return }

    if (profileViewPagersById[profile.uid] == null) {
      profileViewPagersById[profile.uid] = createProfileViewPager(profile)
    }

    profileViewPagerContainer.replaceContent(profileViewPagersById[profile.uid])
  }

  const footer = createPortraitFooter(profileCardClickHandler)
  const column = createLandscapeColumn(profileCardClickHandler)

  const screen = createElement('div', null, [
    createScreenHeader(initialActiveProfile),
    createElement('div', { style: { display: 'flex', flexDirection: 'row' } }, [
      column,
      profileViewPagerContainer
    ]),
    footer
  ])

  return Object.assign(screen, {
    onMount: () => {
      footer.update()
      column.update()
    }
  })
}

/**
 * @param {Profile} activeProfile
 */
function createScreenHeader (activeProfile) {
  const profileCard = createProfileCard(activeProfile,
    () => Action.navigateToScreen(Screen.chooseActiveProfile()), '???',
    { style: { marginLeft: Spacing.layout(3) } })
  Action.addActiveProfileChangeHandler(profileCard.update)

  const header = createHeader([
    profileCard,
    createElement('h5', thisElement => {
      Action.addActiveProfileChangeHandler(profile => {
        if (profile) { thisElement.replaceContent(`${profile.cash} 💰`) }
      })
      return {}
    }, `${activeProfile.cash}💰`),
    createElement('i', {
      className: 'material-icons',
      style: {
        fontSize: Size.icon(2),
        cursor: 'pointer',
        marginRight: Spacing.layout(3)
      },
      onclick: () => Action.navigateToScreen(Screen.settings())
    }, 'settings')
  ])

  return header
}

/**
 * @param {(profile: Profile) => void} onProfileCardClick
 */
function createProfileCards (onProfileCardClick) {
  const profileIds = Action.getProfileIds() || []

  const profiles = profileIds.reduce(/** @param {Profile[]} result */(result, uid) => {
    const profile = Action.getProfile(uid)
    return profile ? result.concat(profile) : result
  }, [])

  return profiles.map(profile => createProfileCard(profile, onProfileCardClick, '???', {
    style: {
      marginLeft: Spacing.layout(1),
      marginBottom: Spacing.layout(1)
    }
  }))
}

/**
 * @param {(profile: Profile) => void} onProfileCardClick
 */
function createPortraitFooter (onProfileCardClick) {
  const profileCardsContainer = createElement('div', {
    style: {
      display: 'flex',
      flexDirection: 'row'
    }
  }, createProfileCards(onProfileCardClick))

  const footer = createElement('div', {
    className: 'portrait-only',
    style: {
      paddingBottom: Spacing.layout(12)
    }
  }, createElement('footer', {
    style: {
      position: 'fixed',
      left: Spacing.nil,
      bottom: Spacing.nil,
      maxWidth: '100%',
      overflowX: 'auto'
    }
  }, profileCardsContainer))

  return Object.assign(footer, {
    update: () => {
      profileCardsContainer.replaceContent(createProfileCards(onProfileCardClick))
    }
  })
}

/**
 * @param {(profile: Profile) => void} onProfileCardClick
 */
function createLandscapeColumn (onProfileCardClick) {
  const profileCardsContainer = createElement('div', {
    style: {
      display: 'flex',
      flexDirection: 'column',
      flex: '1 1 auto',
      overflowY: 'auto'
      // paddingTop: Spacing.layout(2)
    }
  }, createProfileCards(onProfileCardClick))

  const column = createElement('div', {
    className: 'landscape-only',
    style: {
      paddingLeft: Spacing.layout(20)
    }
  }, createElement('div', {
    style: {
      position: 'fixed',
      left: Spacing.nil
      // height: '100%',
      // display: 'flex',
      // flex: '1 1 auto',
      // overflowY: 'auto'
    }
  }, profileCardsContainer))

  return Object.assign(column, {
    update: () => {
      profileCardsContainer.replaceContent(createProfileCards(onProfileCardClick))
    }
  })
}
