import { createElement } from '../packages/element/index.js'
import { Color, Spacing } from './theme.js'
import { index, t } from './translations.js'
import { createViewPager } from './view-pager.js'

/**
 * @param {import('../data/profile').Profile} profile
 */
export function createProfileViewPager (profile) {
  const profileViewPager = createViewPager([
    createElement('div', {
      style: {
        padding: Spacing.layout(2),
        backgroundColor: profile.color
      }
    },
    [
      createElement('h2', null, 'Missions'),
      createElement('pre', null, JSON.stringify(profile, null, 4))
    ]
    ),
    createElement('div', {
      style: {
        padding: Spacing.layout(2),
        backgroundColor: '#afa',
        height: '1200px'

      }
    }, 'Rewards'),
    createElement('div', {
      style: {
        padding: Spacing.layout(2),
        backgroundColor: '#aaf'
      }
    }, 'History')
  ])

  const header = createViewPagerHeader(profileViewPager.goToPageIndex, profileViewPager.addPageChangeListener)

  return createElement('div', null, [
    header,
    profileViewPager
  ])
}

/**
 * @param {(pageIndex: number) => void} goToPageIndex
 * @param {(handler: (pageIndex: number) => void) => void} addPageChangeListener
 */
function createViewPagerHeader (goToPageIndex, addPageChangeListener) {
  const tabs = [
    createProfileViewPagerTab(t(index.missions), () => goToPageIndex(0)),
    createProfileViewPagerTab(t(index.rewards), () => goToPageIndex(1)),
    createProfileViewPagerTab(t(index.events), () => goToPageIndex(2))
  ]

  addPageChangeListener(pageIndex => {
    tabs.forEach(tab => tab.unfocus())
    tabs[pageIndex].focus()
  })

  const header = createElement('main', {
    style: {
      margin: '0 auto',
      width: '100%'
    }
  },
  createElement('div', {
    style: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-evenly'
    }
  }, tabs)
  )

  return header
}

/**
 * @param {string} name
 * @param {() => void} onClick
 */
function createProfileViewPagerTab (name, onClick) {
  const tab = createElement('h5', {
    style: {
      border: 'solid',
      textAlign: 'center',
      flexGrow: '1',
      cursor: 'pointer',
      backgroundColor: Color.secondary
    },
    onclick: onClick
  },
  name)

  return Object.assign(tab, {
    focus: () => {
      tab.style.backgroundColor = Color.primary
    },
    unfocus: () => {
      tab.style.backgroundColor = Color.secondary
    }
  })
}
