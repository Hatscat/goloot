/* eslint-env jest */
import { createProfile } from '../data/index.js'
import { createRepository } from './repository.js'
import { resetAllScreens, routes, Screen } from './routes.js'
import { updater } from './updater.js'

/**
 * @typedef {import('../data/profile').Profile} Profile
 */

jest.mock('./routes.js', () => ({
  resetAllScreens: jest.fn(),
  routes: {
    main: document.createElement('div'),
    settings: document.createElement('div'),
    editProfile: Object.assign(document.createElement('div'), {
      onMount: jest.fn()
    })
  },
  Screen: {
    main: () => ({ name: 'main' }),
    settings: () => ({ name: 'settings' }),
    /** @param {Profile} profile */
    editProfile: profile => ({ name: 'editProfile', param: profile })
  }
}))
jest.mock('../data/index.js', () => ({
  defineNavigatorLanguage: () => 'fr',
  /** @param  {Partial<Profile>?} [props] */
  createProfile: props => ({ uid: String(Math.random()), ...props })
}))

describe('updater', () => {
  describe('updater()', () => {
    // setup
    /** @type {Record<string, any>} */
    const memory = {}
    const memoryStorageAdapter = {
      /** @param {string} key */
      get: key => memory[key] || null,
      /** @param {string} key @param {any} value */
      set: (key, value) => { memory[key] = value },
      /** @param {string} key */
      remove: key => { delete memory[key] },
      clear: () => {
        const memoryKeys = Object.keys(memory)
        for (let i = 0; i < memoryKeys.length; i += 1) {
          delete memory[memoryKeys[i]]
        }
      }
    }
    const repository = createRepository(memoryStorageAdapter)
    const rootElement = document.createElement('div')
    const Action = updater(repository, rootElement)

    beforeEach(() => {
      repository.removeEverything()
    })

    // tests
    describe('Action.refreshAllScreens()', () => {
      test('should navigate to "main" screen if no active screen', () => {
        Action.refreshAllScreens()
        expect(rootElement.children[0]).toBe(routes.main)
      })
      test('should call routes.resetAllScreens()', () => {
        Action.refreshAllScreens()
        expect(resetAllScreens).toBeCalledTimes(1)
      })
      test('should navigate to latests active screen', () => {
        Action.navigateToScreen(Screen.settings())
        Action.refreshAllScreens()
        expect(rootElement.children[0]).toBe(routes.settings)
        Action.navigateToScreen(Screen.main())
        Action.refreshAllScreens()
        expect(rootElement.children[0]).toBe(routes.main)
      })
    })

    describe('Action.navigateToScreen()', () => {
      test('should update the repository screenNameStack', () => {
        Action.navigateToScreen(Screen.main())
        expect(history.state.name).toBe('main')
        Action.navigateToScreen(Screen.settings())
        expect(history.state.name).toBe('settings')
      })
      test('should change root element children', () => {
        Action.navigateToScreen(Screen.main())
        Action.navigateToScreen(Screen.settings())
        expect(rootElement.childElementCount).toBe(1)
        expect(rootElement.children[0]).toBe(routes.settings)
      })
      test('should call onMount(params) if present in route element', () => {
        const profile = createProfile()
        Action.navigateToScreen(Screen.editProfile(profile))
        expect(rootElement.children[0]).toBe(routes.editProfile)
        expect(routes.editProfile.onMount).toHaveBeenCalledTimes(1)
        expect(routes.editProfile.onMount).toHaveBeenCalledWith(profile)
      })
    })

    // window.history isn't fully supported by Jest DOM, tests to remove?
    describe.skip('Action.navigateBack()', () => {
      test('should update the repository screenNameStack', () => {
        const aProfile = createProfile()
        Action.navigateToScreen(Screen.editProfile(aProfile))
        Action.navigateToScreen(Screen.main())
        Action.navigateToScreen(Screen.settings())
        expect(history.state.name).toBe('settings')
        Action.navigateBack()
        expect(history.state.name).toBe('main')
        Action.navigateBack()
        expect(history.state).toEqual({ name: 'editProfile', param: aProfile })
      })
    })

    describe('Action.getOrCreateLocale()', () => {
      test('should return navigator language if nothing in repository', () => {
        const locale = Action.getOrCreateLocale()
        expect(locale).toBe('fr')
      })
    })

    describe('Action.setLocale()', () => {
      test('should set locale in repository and call resetAllScreens()', () => {
        Action.setLocale('en')
        expect(repository.locale.get()).toBe('en')
        expect(resetAllScreens).toBeCalledTimes(1)
      })
    })

    describe('Action.getProfileIds()', () => {
      test('should return null if no stored profile', () => {
        expect(Action.getProfileIds()).toEqual(null)
      })
      test('should return stored profiles ids', () => {
        const ids = ['a', 'b', 'c']
        memory.profile_ids = ids
        expect(Action.getProfileIds()).toEqual(ids)
      })
    })

    describe('Action.getProfile(id)', () => {
      test('should return null if no stored profile for the given id', () => {
        expect(Action.getProfile('42')).toEqual(null)
      })
      test('should return the requested profile when id was given', () => {
        const aProfile = createProfile({ uid: '456test123test' })
        repository.profile.set(aProfile)
        repository.profileIds.push(aProfile.uid)

        const profile = Action.getProfile(aProfile.uid)
        expect(profile).toEqual(aProfile)
      })
    })

    describe('Action.editProfile(uid, props)', () => {
      test('should partially update existing profile.', () => {
        const aProfile = createProfile()
        repository.profile.set(aProfile)
        repository.profileIds.push(aProfile.uid)
        const name = 'editProfileTest'
        Action.editProfile(aProfile.uid, { name })
        const storedProfile = repository.profile.get(aProfile.uid)
        expect(storedProfile != null && storedProfile.name).toBe(name)
      })
      test('should do nothing for values that are empty.', () => {
        const profileProps = { name: 'bob', emoji: '😎' }
        const aProfile = createProfile(profileProps)
        repository.profile.set(aProfile)
        repository.profileIds.push(aProfile.uid)
        const color = '#FFF'
        Action.editProfile(aProfile.uid, { name: null, emoji: '', color })
        const storedProfile = repository.profile.get(aProfile.uid)
        expect(storedProfile).toEqual(createProfile({ ...profileProps, uid: aProfile.uid, color }))
        expect(storedProfile != null && storedProfile.name).not.toEqual(null)
        expect(storedProfile != null && storedProfile.emoji).not.toEqual('')
      })
      test('should create a new profile if uid is unknown in storage.', () => {
        const profileProps = { name: 'bob', emoji: '😎' }
        const aProfile = createProfile()
        Action.editProfile(aProfile.uid, profileProps)
        const storedProfile = repository.profile.get(aProfile.uid)
        expect(storedProfile).toEqual(createProfile({ ...profileProps, uid: aProfile.uid }))
        expect(repository.profileIds.get()).toEqual([aProfile.uid])
      })
    })

    describe('Action.getOrCreateActiveProfile()', () => {
      test('should return a new stored profile when no id was given', () => {
        const profile = Action.getOrCreateActiveProfile()
        expect(profile).not.toBeFalsy()
        expect(profile).toEqual(Action.getProfile(profile.uid))
        const profileIds = Action.getProfileIds()
        expect(profileIds != null && profileIds.includes(profile.uid)).toBe(true)
        expect(profile.uid).toBe(memory.active_profile_id)
      })
      test('should return the active profile when no id was given', () => {
        const activeProfile = createProfile({ uid: 'test123' })
        repository.profile.set(activeProfile)
        repository.profileIds.push(activeProfile.uid)
        repository.activeProfileId.set(activeProfile.uid)

        const profile = Action.getOrCreateActiveProfile()
        expect(profile).toEqual(activeProfile)
      })
    })

    describe('Action.setActiveProfileId(id)', () => {
      test('should set the given id as activeProfileId', () => {
        const aProfile = createProfile({ uid: '456test123test' })
        repository.profile.set(aProfile)

        Action.setActiveProfileId(aProfile.uid)

        expect(repository.activeProfileId.get()).toEqual(aProfile.uid)
        expect(Action.getOrCreateActiveProfile().uid).toEqual(aProfile.uid)
      })
    })

    describe('Action.addActiveProfileChangeHandler(handler)', () => {
      test('should trigger an added handler if active profile id change', () => {
        let activeProfile = null
        const obj = {
        /** @param {Profile | null} profile */
          handler: profile => { activeProfile = profile }
        }
        jest.spyOn(obj, 'handler')
        Action.addActiveProfileChangeHandler(obj.handler)
        expect(activeProfile).toEqual(null)

        const newActiveProfile = Action.getOrCreateActiveProfile()
        expect(activeProfile).toEqual(newActiveProfile)

        const newId = 'anything42'
        Action.editProfile(newId, { name: 'anyName' })
        Action.setActiveProfileId(newId)
        expect(activeProfile).toEqual(repository.profile.get(newId))

        expect(obj.handler).toHaveBeenCalledTimes(2)
      })
      test('should trigger an added handler when active profile was updated', () => {
        /** @type {Profile | null} */
        let activeProfile = null
        const obj = {
        /** @param {Profile | null} profile */
          handler: profile => { activeProfile = profile }
        }
        jest.spyOn(obj, 'handler')
        Action.addActiveProfileChangeHandler(obj.handler)
        expect(activeProfile).toEqual(null)

        const newActiveProfile = Action.getOrCreateActiveProfile()
        expect(activeProfile).toEqual(newActiveProfile)

        Action.editProfile(newActiveProfile.uid, { name: 'anyName' })
        expect(/** @type {any} */ (activeProfile).name).toEqual('anyName')

        Action.editProfile(newActiveProfile.uid, { cash: 42 })
        expect(/** @type {any} */ (activeProfile).cash).toEqual(42)

        expect(obj.handler).toHaveBeenCalledTimes(3)
      })
    })

    describe('Action.removeAllStoredData()', () => {
      test('should remove everything from repository / storage', () => {
        repository.locale.set('en')
        Action.getOrCreateActiveProfile()

        expect(Object.keys(memory).length).toBe(1 + 3)

        Action.removeAllStoredData()
        expect(Object.keys(memory).length).toBe(0)
      })
    })
  })
})
