import { createProfile, defineNavigatorLanguage } from '../data/index.js'
import { resetAllScreens, routes, Screen } from './routes.js'

/**
 * @typedef {import('./routes').ScreenName} ScreenName
 * @typedef {import('./routes').ScreenState} ScreenState
 * @typedef {import('../data/profile').Profile} Profile
 */

/**
 *
 * @param {import('./repository').Repository} repository
 * @param {HTMLElement | null} rootHtmlElement
 */
export function updater (repository, rootHtmlElement) {
  const cleanScreen = () => {
    if (rootHtmlElement) {
      while (rootHtmlElement.firstChild) {
        rootHtmlElement.removeChild(rootHtmlElement.firstChild)
      }
    }
  }

  /**
   * @param {PopStateEvent | {state: ScreenState | null}} event
   */
  const popStateEventHandler = event => {
    /** @type {ScreenState} */
    const screenState = event.state || Screen.main()

    if (rootHtmlElement) {
      cleanScreen()
      const screenElement = routes[screenState.name]
      if (screenElement.onMount) {
        screenElement.onMount(screenState.param)
      }
      rootHtmlElement.appendChild(screenElement)
    }
  }

  /**
   * @param {ScreenState} screenState
   */
  const navigateToScreen = screenState => {
    history.pushState(screenState, screenState.name)
    popStateEventHandler({ state: screenState })
  }

  const navigateBack = () => {
    history.back()
  }

  const refreshAllScreens = () => {
    resetAllScreens()
    popStateEventHandler({ state: history.state })
  }

  const getOrCreateLocale = () => {
    const storedLocale = repository.locale.get()
    if (storedLocale != null) {
      return storedLocale
    }
    const locale = defineNavigatorLanguage()
    repository.locale.set(locale)

    return locale
  }

  /**
   * @param {import('../data/locale').Locale} locale
   */
  const setLocale = locale => {
    repository.locale.set(locale)
    refreshAllScreens()
  }

  /**
   * @param  {Partial<Profile>?} [props]
   */
  const addNewProfile = props => {
    const profile = createProfile(props)
    repository.profile.set(profile)
    repository.profileIds.push(profile.uid)
    return profile
  }

  /**
   * @param {Profile['uid']} uid
   * @param {Partial<Profile>} props
   */
  const editProfile = (uid, props) => {
    (/** @type {(keyof Profile)[]} */ (Object.keys(props))).forEach(key => {
      if (props[key] == null || props[key] === '') {
        delete props[key]
      }
    })
    if (Object.values(props).length > 0) {
      const profile = repository.profile.get(uid)
      if (profile) {
        repository.profile.set({ ...profile, ...props })
      } else {
        addNewProfile({ ...props, uid })
      }
    }
  }

  /**
   * @returns {Profile}
   */
  const getOrCreateActiveProfile = () => {
    const activeProfileId = repository.activeProfileId.get()
    if (activeProfileId != null) {
      const maybeProfile = repository.profile.get(activeProfileId)
      if (maybeProfile) return maybeProfile
    }

    const profile = addNewProfile()
    repository.activeProfileId.set(profile.uid)

    return profile
  }

  /**
   * @param {(value: Profile | null) => void} handler
   */
  function addActiveProfileChangeHandler (handler) {
    let activeProfileId = repository.activeProfileId.get()
    if (activeProfileId) {
      repository.profile.watch(handler, activeProfileId)
    }
    repository.activeProfileId.watch(profileId => {
      if (profileId) {
        if (activeProfileId) {
          repository.profile.unwatch(handler, activeProfileId)
        }
        repository.profile.watch(handler, profileId)
        handler(repository.profile.get(profileId))
      } else {
        handler(null)
      }
      activeProfileId = profileId
    })
  }

  return {
    // alphabetically sorted
    addActiveProfileChangeHandler,
    // TODO: add small test
    addProfileChangeHandler: repository.profile.watch,
    editProfile,
    getOrCreateActiveProfile,
    getOrCreateLocale,
    getProfile: repository.profile.get,
    getProfileIds: repository.profileIds.get,
    navigateToScreen,
    navigateBack,
    // TODO: to test
    popStateEventHandler,
    refreshAllScreens,
    removeAllStoredData: repository.removeEverything,
    setActiveProfileId: repository.activeProfileId.set,
    setLocale
  }
}
