import Config from '../config.js'
import LocalStorageAdapter from '../packages/storage/local-storage-adapter.js'
import { createRepository } from './repository.js'
import { updater } from './updater.js'

const repository = createRepository(LocalStorageAdapter)
const rootHtmlElement = document.getElementById(Config.rootHtmlElementId)

const Action = updater(repository, rootHtmlElement)

export { Screen } from './routes.js'
export { Action as default }
