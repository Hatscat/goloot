import { createChooseActiveProfileScreen, createEditProfileScreen, createMainScreen, createSettingsScreen } from '../view/index.js'

/**
 * @typedef {import('../packages/element/index').Lm} Lm
 * @typedef {import('../data/profile').Profile} Profile
 */

/**
 * @template {string} Name
 * @template {any} Param
 * @typedef {{name: Name, param: Param}} ScreenType
 */
/**
 * @typedef {ScreenType<"main", void>} MainScreen
 * @typedef {ScreenType<"settings", void>} SettingScreen
 * @typedef {ScreenType<"chooseActiveProfile", void>} ChooseActiveProfileScreen
 * @typedef {ScreenType<"editProfile", Profile>} EditProfileScreen
 */
/**
 * @typedef {(MainScreen | SettingScreen | ChooseActiveProfileScreen | EditProfileScreen)} ScreenState
 */

export const Screen = {
  /** @returns {MainScreen} */
  main () {
    return { name: 'main', param: undefined }
  },
  /** @returns {SettingScreen} */
  settings () {
    return { name: 'settings', param: undefined }
  },
  /** @returns {ChooseActiveProfileScreen} */
  chooseActiveProfile () {
    return { name: 'chooseActiveProfile', param: undefined }
  },
  /**
   * @param {Profile} profile
   * @returns {EditProfileScreen}
   */
  editProfile (profile) {
    return { name: 'editProfile', param: profile }
  }
}

/**
 * @typedef {ScreenState['name']} ScreenName
 */
/**
 * @typedef {Record<ScreenState['name'], Lm & {onMount?: function}>} Routes
 */

/** @type {Partial<Routes>} */
const memory = {}

/** @type {Routes} */
export const routes = {
  get main () {
    if (memory.main == null) { memory.main = createMainScreen() }
    return memory.main
  },
  get settings () {
    if (memory.settings == null) { memory.settings = createSettingsScreen() }
    return memory.settings
  },
  get chooseActiveProfile () {
    if (memory.chooseActiveProfile == null) { memory.chooseActiveProfile = createChooseActiveProfileScreen() }
    return memory.chooseActiveProfile
  },
  get editProfile () {
    if (memory.editProfile == null) { memory.editProfile = createEditProfileScreen() }
    return memory.editProfile
  }
}

/**
 * Reset screen element
 * @param {ScreenName} screenName
 */
export function resetScreen (screenName) {
  memory[screenName] = undefined
}

export function resetAllScreens () {
  /** @type {ScreenName[]} */
  // @ts-ignore
  const screenNames = Object.keys(memory)
  screenNames.forEach(screenName => { memory[screenName] = undefined })
}
