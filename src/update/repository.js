
import { storage } from '../packages/storage/index.js'

/**
 * @typedef {import('./routes').ScreenName} ScreenName
 * @typedef {import('../packages/translator').Locale} Locale
 * @typedef {import('../data/index').Profile} Profile
 */
/**
 * @template {any} Value
 * @typedef {import('../packages/storage').Callback<Value>} WatchCallback
 */

/**
 * @template {string | void} Key
 * @template {any} Value
 * @typedef {{get: (key: Key) => Value | null}} Get
 */
/**
 * @template {any} Value
 * @typedef {{set: (value: Value) => void}} Set
 */
/**
 * @template {string | void} Key
 * @typedef {{remove: (key: Key) => void}} Remove
 */

/**
 * @template {any} Value
 * @typedef {{peek: () => Value | null}} Peek
 */
/**
 * @template {any} Value
 * @typedef {{pop: () => Value | null}} Pop
 */
/**
 * @template {any} Value
 * @typedef {{push: (value: Value) => void}} Push
 */

/**
 * @template {any} Value
 * @template {string | void} Key
 * @typedef {{watch: (handler: WatchCallback<Value>, key: Key) => void}} Watch
 */

/**
 * @template {any} Value
 * @template {string | void} Key
 * @typedef {{unwatch: (handler: WatchCallback<Value>, key: Key) => void}} Unwatch
 */

/**
 * @typedef {Object} Repository
 * @property {() => void} removeEverything
 * @property {Get<void, Locale> & Set<Locale> & Watch<Locale, void>} locale
 * @property {Get<void, string> & Set<string> & Watch<string, void>} activeProfileId
 * @property {Get<void, string[]> & Push<string> & Remove<string> & Watch<string[], void>} profileIds
 * @property {Get<string, Profile> & Set<Profile> & Remove<string> & Watch<Profile, string> & Unwatch<Profile, string>} profile
 */

/**
  * @param {import('../packages/storage').StorageAdapter} storageAdapter
  */
export function createRepository (storageAdapter) {
  const store = storage(storageAdapter)

  /**
   * @type {Repository}
   * @readonly
   */
  const repository = {
    removeEverything: store.clear,
    locale: {
      get: () => /** @type {Locale | null} */ (store.get('locale')),
      set: locale => store.set('locale', locale),
      watch: handler => store.addDataUpdateListener('locale', handler)
    },
    activeProfileId: {
      get: () => /** @type {string | null} */ (store.get('active_profile_id')),
      set: profileId => store.set('active_profile_id', profileId),
      watch: handler => store.addDataUpdateListener('active_profile_id', handler)
    },
    profileIds: {
      get: () => /** @type {string[] | null} */ (store.get('profile_ids')),
      push: profileId => store.set('profile_ids', /** @type {string[]} */ (store.get('profile_ids') || []).concat(profileId)),
      remove: profileId => {
        const profileIds = /** @type {string[] | null} */ (store.get('profile_ids'))
        if (profileIds != null && profileIds.includes(profileId)) {
          store.set('profile_ids', profileIds.filter(id => id !== profileId))
        }
      },
      watch: handler => store.addDataUpdateListener('profile_ids', handler)
    },
    profile: {
      get: uid => /** @type {Profile | null} */ (store.get(uid)),
      set: profile => store.set(profile.uid, profile),
      remove: uid => store.remove(uid),
      watch: (handler, uid) => store.addDataUpdateListener(uid, handler),
      unwatch: (handler, uid) => store.removeDataUpdateListener(uid, handler)
    }
  }

  return repository
}
