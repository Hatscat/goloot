
import { getNavigatorLanguage } from '../packages/translator/index.js'

/**
 * @typedef {("en" | "fr")} Locale
 */

/**
 * @type {Locale[]}
 * @readonly
 */
export const availableLocales = ['en', 'fr']

export function defineNavigatorLanguage () {
  const navLang = getNavigatorLanguage().slice(0, 2)

  return availableLocales.includes(/** @type {Locale} */(navLang))
    ? /** @type {Locale} */ (navLang)
    : availableLocales[0]
}
