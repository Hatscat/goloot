import { uid } from '../packages/uid/index.js'

/**
 * @typedef {Object} Profile
 * @property {string} uid
 * @property {string | null} name
 * @property {string} emoji
 * @property {string} color
 * @property {number} cash
 * @property {string[]} missionIds
 * @property {string[]} rewardIds
 */

/**
 * @param  {Partial<Profile>?} [props]
 */
export function createProfile (props) {
  /** @type {Profile} */
  const profile = {
    uid: uid(),
    name: null,
    emoji: '👤',
    color: '#FFEE58', // secondary color... to pick in theme file?
    cash: 0,
    missionIds: [],
    rewardIds: [],
    ...props
  }

  return profile
}

/**
 * @param  {Profile} profile
 */
export function isNewProfile (profile) {
  return profile.name == null
}
