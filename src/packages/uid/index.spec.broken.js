/* eslint-env jest */
import { uid } from './index.js'

// currently broken...
// Web Crypto is not implemented in jsdom
const crypto = {
// @ts-ignore
  getRandomValues: array => Array.from(array).fill(0).map(Math.random)
}
Object.defineProperty(window, 'crypto', crypto)
// jest.spyOn(window, 'crypto')

describe('uid', () => {
  describe('uid()', () => {
    test('Result should be unique.', () => {
      expect(uid()).not.toEqual(uid())
    })
  })
})
