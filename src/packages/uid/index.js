/**
 * Generates a unique 128-bit identifier in arithmetic base 36 ([0-9a-z])
 */
export function uid () {
  return Array.from(window.crypto.getRandomValues(new Uint32Array(4))).map(n => n.toString(36)).join('')
}
