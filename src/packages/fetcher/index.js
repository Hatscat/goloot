/**
 * @typedef {( "GET" | "POST" | "PUT" | "PATCH" | "DELETE" )} Method
 * @typedef {( string | number | boolean | null | Object | Array<*> )} JsonValue
 * @typedef {{ path: string, method: Method, headers?: Record<string, string>, body?: Record<string, JsonValue> }} Query
 */

export class FetchError extends Error {
  /**
   * @param {string} message
   * @param {Response} response
   */
  constructor (message, response) {
    super(message)
    this.response = response
  }
}

/**
 * Fetch API functor.
 * @param {string} baseUrl API base URL.
 */
export function fetcher (baseUrl) {
  return {
    /**
     * Fetch JSON content.
     * @param {Query} query
     * @returns {Promise<Record<string, JsonValue>>}
     * @throws {FetchError}
     */
    async fetchJson (query) {
      const requestInit = {
        method: query.method,
        headers: {
          ...query.headers,
          'Content-Type': 'application/json'
        },
        body: query.body ? JSON.stringify(query.body) : undefined
      }
      const response = await fetch(baseUrl + query.path, requestInit)

      if (response.ok) {
        const result = await response.json()

        return result
      } else {
        throw new FetchError(response.statusText, response)
      }
    }
  }
}
