/**
 * Enhanced HTMLElement
 * @typedef {HTMLElement & LmProps} Lm
 *
 * @typedef {Object} LmProps
 * @property {(content: LmContent) => Lm} addContent
 * @property {(content: LmContent) => Lm} replaceContent
 *
 * @typedef {(Lm | HTMLElement | string | null | Array<(Lm | HTMLElement | string | null)>)=} LmContent
 *
 * @typedef {Partial<Element> & {style: Partial<CSSStyleDeclaration>} & Partial<DocumentAndElementEventHandlers> & Partial<GlobalEventHandlers> & Record<string, any>} LmAttributes
 */

/**
 * `document.createElement` enhanced with methods `addContent ` and `replaceContent`.
 * @param {!string} tagName HTMLElement tag name, like `div` or `input`.
 * @param {(Partial<LmAttributes> | ((thisElement: Lm) => Partial<LmAttributes>) | null)} [attributes] any attributes you would like to add, like `className` value or `onChange` event. This parameter can be a function, for example: `(element) => ({ onClick: event => element.className = 'clicked' })`.
 * @param {LmContent} [content] element children nodes.
 * @returns {Lm} element
 */
export function createElement (tagName, attributes, content) {
  /** @type {Lm} */
  const element = Object.assign(document.createElement(tagName), {
    /** @param {LmContent} content */
    addContent: content => addContent(element, content),
    /** @param {LmContent} [content] */
    replaceContent: content => replaceContent(element, content)
  })

  if (attributes != null) {
    const attributeDict = typeof attributes === 'function' ? attributes(element) : attributes

    Object.keys(attributeDict).forEach(key => {
      // @ts-ignore
      if (element[key] != null && typeof element[key] === 'object') {
      // @ts-ignore
        Object.assign(element[key], attributeDict[key])
      } else {
      // @ts-ignore
        element[key] = attributeDict[key]
      }
    })
  }

  return element.addContent(content)
}

/**
 * @param {Lm} element
 * @param {LmContent} content
 * @returns {Lm} element
 */
function addContent (element, content) {
  if (content != null) {
    if (Array.isArray(content)) {
      content.forEach(element.addContent)
    } else if (typeof content === 'object' && content.appendChild != null) {
      element.appendChild(content)
    } else {
      const contentString = typeof content === 'object' ? JSON.stringify(content, null, 2) : String(content)
      element.innerText += contentString
    }
  }
  return element
}

/**
 * @param {Lm} element
 * @param {LmContent} [content]
 * @returns {Lm} element
 */
function replaceContent (element, content) {
  while (element.firstChild) {
    element.removeChild(element.firstChild)
  }
  return element.addContent(content)
}
