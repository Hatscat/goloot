
/**
 * Listen for global error events and trigger errorListener.
 * @param {(error: Error, event: ErrorEvent | PromiseRejectionEvent) => void} errorListener Function called when an error is trigger.
 */
export function setupErrorHandler (errorListener) {
  if (typeof errorListener !== 'function') {
    throw new Error('Invalid errorListener.')
  }
  window.addEventListener('error', event => errorListener(event.error, event))
  window.addEventListener('unhandledrejection', event => {
    const error = event.reason instanceof Error
      ? event.reason
      : new Error(typeof event.reason === 'object' ? JSON.stringify(event.reason) : String(event.reason))
    errorListener(error, event)
  })
}
