/**
 * @typedef {("get" | "set" | "remove" | "clear")} StorageAction
 */

/** @type {Readonly<{ Get: "get"; Set: "set"; Remove: "remove"; Clear: "clear"; }>} */
export const StorageAction = {
  Get: 'get',
  Set: 'set',
  Remove: 'remove',
  Clear: 'clear'
}

/**
 * @typedef {{ get: (key: string) => unknown | null, set: (key: string, value: unknown) => void, remove: (key: string) => void, clear: () => void }} StorageAdapter
 */
/**
 * @template {any} Value
 * @typedef {(value: Value | null, key: string, action: StorageAction) => void} Callback
 */

/**
 * Setup storage functor with an adapter.
 * @param {StorageAdapter} storageAdapter
 * @returns {{ addDataUpdateListener: (key: string, handler: Callback<any>) => void, removeDataUpdateListener: (key: string, handler: Callback<any>) => void } & StorageAdapter}
 */
export function storage (storageAdapter) {
  /** @type {{ [key: string]: Callback<any>[] }} */
  const listeners = {}

  return {
    /**
     * @template {any} Value
     * @param {string} key
     * @param {Callback<Value>} callback
     */
    addDataUpdateListener: (key, callback) => {
      listeners[key] = Array.isArray(listeners[key]) ? listeners[key].concat(callback) : [callback]
    },
    removeDataUpdateListener: (key, callback) => {
      if (Array.isArray(listeners[key])) {
        listeners[key] = listeners[key].filter(fun => fun !== callback)
      }
    },
    get: storageAdapter.get,
    set: (key, value) => {
      storageAdapter.set(key, value)
      if (Array.isArray(listeners[key])) {
        listeners[key].forEach(callback => callback(value, key, StorageAction.Set))
      }
    },
    remove: key => {
      storageAdapter.remove(key)
      if (Array.isArray(listeners[key])) {
        listeners[key].forEach(callback => callback(null, key, StorageAction.Remove))
      }
    },
    clear: () => {
      storageAdapter.clear()
      Object.keys(listeners).forEach(key => {
        listeners[key].forEach(callback => callback(null, key, StorageAction.Clear))
        delete listeners[key]
      })
    }
  }
}
