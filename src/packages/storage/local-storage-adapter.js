const LocalStorageAdapter = {
  /**
   * Get stored value.
   * @template {any} Value
   * @param {string} key
   * @returns {Value}
   */
  get: function (key) {
    const jsonValue = window.localStorage.getItem(key)

    return jsonValue ? JSON.parse(jsonValue).value : null
  },
  /**
   * Store a value at given key.
   * @template {any} Value
   * @param {string} key
   * @param {Value} value
   */
  set: function (key, value) {
    const jsonValue = JSON.stringify({ value })

    window.localStorage.setItem(key, jsonValue)
  },
  /**
   * Remove stored value.
   * @param {string} key
   */
  remove: function (key) {
    window.localStorage.removeItem(key)
  },
  clear: function () {
    window.localStorage.clear()
  }
}

export default LocalStorageAdapter
