/* eslint-env jest */
import { translator } from './index.js'

describe('translator', () => {
  describe('translator()', () => {
    const indexKeys = {
      hi: '',
      bye: ''
    }

    const fr = {
      hi: 'salut !',
      bye: 'au revoir.'
    }

    const en = {
      hi: 'hello world!',
      bye: 'see you later.'
    }

    test('should return english translations', () => {
      const { t, index } = translator(indexKeys, { fr, en }, () => 'en')
      expect(t(index.hi)).toBe(en.hi)
      expect(t(index.bye)).toBe(en.bye)
    })

    test('should return french translations', () => {
      const { t, index } = translator(indexKeys, { fr, en }, () => 'fr')
      expect(t(index.hi)).toBe(fr.hi)
      expect(t(index.bye)).toBe(fr.bye)
    })

    test('should return empty string if no available language', () => {
      // @ts-ignore
      const { t, index } = translator(indexKeys, { fr, en }, () => '')
      expect(t(index.hi)).toBe('')
      expect(t(index.bye)).toBe('')
    })
  })
})
