import { getNavigatorLanguage, translator } from '../index.js'

/**
 * @typedef {import('../index').Locale} Locale
 */

const indexKeys = {
  hi: '',
  bye: ''
}

const fr = {
  hi: 'salut !',
  bye: 'au revoir.'
}

const en = {
  hi: 'hello world!',
  bye: 'see you later.'
}

/**
 * @returns {Locale}
 */
function getUserLanguage () {
  /** @type {Locale[]} */
  const locales = ['en', 'fr']
  // try to load from store, else:
  const navLang = getNavigatorLanguage().slice(0, 2)
  return locales.includes(/** @type {Locale} */ (navLang)) ? /** @type {Locale} */ (navLang) : locales[0]
}

const { t, index } = translator(indexKeys, { fr, en }, getUserLanguage)

console.log(t(index.hi), t(index.bye))
