/**
 * @typedef {("en" | "fr")} Locale
 */

/**
 * Translate functor.
 * @template {string} Key
 * @param {Record<Key, string>} indexKeys
 * @param {Record<Locale, Record<Key, string>>} dictionary
 * @param {() => Locale} getLocale
 * @returns {{t: (key: Key) => string, index: Record<Key, Key>}}
 */
export function translator (indexKeys, dictionary, getLocale) {
  /**
   * Get translation in locale dictionary by key.
   * @param {Key} key
   */
  const t = key => {
    const locale = getLocale()
    if (dictionary[locale] == null) return ''
    return dictionary[locale][key]
  }

  Object.keys(indexKeys).forEach(key => { indexKeys[/** @type {Key} */ (key)] = key })

  return { t, index: /** @type {Record<Key, Key>} */(indexKeys) }
}

/**
 * @returns {Locale | string}
 */
export function getNavigatorLanguage () {
  if (navigator.languages != null) {
    return navigator.languages[0]
  } else if (navigator.language != null) {
    return navigator.language
  } else {
    return 'en'
  }
}
