export const Environment = {
  Dev: 'DEV',
  PreProd: 'PROD',
  Prod: 'PROD'
}

const Config = {
  rootHtmlElementId: 'app',
  env: Environment.Dev,
  get isDevMode () {
    return /DEV_MODE$/.test(location.hash)
  }
}

export default Config
