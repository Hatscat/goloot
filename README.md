# Goloot

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)

## [Demo](https://hatscat.gitlab.io/goloot/)

## Technical stack

  > This project aim to be [buildless](https://dev.to/open-wc/generating-typescript-definition-files-from-javascript-5bp2), without any framework, and as library less as possible, ideally anyone should be able to launch the app just after cloning the repository, without any setup or build step.

* UI: plain [JavaScript (ES6)](https://developer.mozilla.org/fr/docs/Web/JavaScript)
* Data storage: [localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API)
* Data flow: plain [JavaScript (ES6)](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
* Style: plain [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS)
* Typing: [JSDoc](https://jsdoc.app/) + [TypeScript](https://www.typescriptlang.org/)
* Mobile (offline): [PWA](https://developers.google.com/web/progressive-web-apps/)
* Bundle: [rollup](https://rollupjs.org/guide/en/)
* Unit test: [Jest](https://jestjs.io/)
* E2E test: *not yet, maybe cypress.io*
* Analytics: *not yet, maybe [google analytics](https://analytics.google.com/analytics/web/)*

## Development prerequisites

* [Node.js (for npm)](https://nodejs.org)

## Installation

To install all dependencies, run the following command from the project directory:

```sh
npm install
```

### Recommended IDE

[Visual Studio Code](https://code.visualstudio.com/)

## Run the app on web browser

```sh
npm run dev
```

Then open your browser at [`localhost:3000`](http://localhost:3000)

## Check the code (eslint + typescript)

```sh
npm run check
```

## Run tests

```sh
npm t
```

## Check eslint and typescript + run tests

```sh
npm run validate
```

## Fix dev environment issues

```sh
npm run rescue
```
