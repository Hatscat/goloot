# Project setup steps

## requirements

* NodeJs

## commands

```sh
npm init
git init
```

```sh
# OLD WAY: npm i -D prettier eslint eslint-config-prettier eslint-plugin-prettier eslint-plugin-jest eslint-plugin-import eslint-plugin-jsdoc eslint-plugin-unicorn eslint-plugin-promise eslint-plugin-filenames
npm i -D eslint eslint-config-standard eslint-plugin-standard eslint-plugin-promise eslint-plugin-import eslint-plugin-node
npm i -D typescript jsdoc
npm i -D husky jest npm-run-all
npm i -D lite-server
npm i -D @types/jest ts-jest
npx jest --init
```
