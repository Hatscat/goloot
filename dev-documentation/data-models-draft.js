export const account = {
  uid: 'efcd-39',
  authToken: 'aefdcb-23', // ? (handle by firestore)
  emails: [
    'bob.banane@gmail.com',
    'jane.banane@gmail.com'
  ],
  premiumFeatures: {
    maxProfileCount: 3,
    unlimitedComments: true,
    noAds: true
  },
  // not persisted ! local only
  activeProfileId: '587-3a439'
}

export const profile = {
  uid: '587-3a439',
  name: 'bob',
  avatar: '🌴',
  color: '#8ae2cb',
  cash: 21,
  missionIds: ['af42'],
  rewardIds: ['af45']
}

export const mission = {
  uid: 'af42',
  name: 'clean the deck',
  recurring: false,
  cashReward: 2,
  priority: 'HIGH', // 'LOW' | 'MEDIUM' | 'HIGH'
  description: 'remove trash'
  // reminders // ?? (local device only, data stored? maybe in a separated model?)
  // assignedProfileIds: ['587-3a439'] // ?? // no need i think, same for rewards
}

export const reward = {
  uid: 'af45',
  name: 'play video game',
  recurring: true,
  cashCost: 3,
  description: '30 minutes max session', // string | null
  expirationTimestamp: null // number | null
}

// event : [qui] à fait [quoi] à [qui] ? + [diff] details

export const createRewardEvent = {
  uid: 'ac4-cc2',
  v: 1, // version
  created: 1580915121424, // timestamp
  synced: 1580915121424,
  authorId: '587-3a439',
  targetId: '587-3a439',
  // proposal: false, // no, to set in type instead
  type: 'CREATE_REWARD', // 'CREATE' | 'UPDATE' | 'DELETE'
  data: {
    uid: 'af45',
    name: 'play video game',
    recurring: true,
    cashCost: 3,
    description: '30 minutes max session'
  }
}

export const updateRewardEvent = {
  uid: 'ac4-cc2',
  v: 1,
  created: 1580915121424,
  synced: 1580915121424,
  authorId: '587-3a439',
  targetId: '587-3a439',
  type: 'UPDATE_REWARD',
  data: {
    uid: 'af45',
    name: 'Clean the Deck!',
    description: 'Remove ALL trash!'
  }
}

export const deleteRewardEvent = {
  uid: 'ac4-cc2',
  v: 1,
  created: 1580915121424,
  synced: 1580915121424,
  authorId: '587-3a439',
  targetId: '587-3a439',
  type: 'DELETE_REWARD',
  data: {
    uid: 'af45'
  }
}

// OLD:
// export const updateModelEvent = {
//   uid: 'ba-ac42',
//   authorId: '587-3a439',
//   targetId: '587-3a439',
//   proposal: false,
//   timestamp: 1580917148652,
//   type: 'UPDATE',
//   modelId: 'af45',
//   modelType: 'REWARD', // needed?
//   modelPropsDiff: {
//     name: 'Clean the Deck!',
//     description: 'Remove ALL trash!'
//   }
// }

// export const deleteModelEvent = {
//   uid: 'ffa-244',
//   authorId: '587-3a439',
//   targetId: '587-3a439',
//   proposal: false,
//   timestamp: 1580917540957,
//   type: 'DELETE',
//   modelId: 'af45'
// }

export function uid () {
  return Array.from(crypto.getRandomValues(new Uint32Array(4))).map(n => n.toString(36)).join('')
}
